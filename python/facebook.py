# Licences Creative Commons : CC-BY-NC
# DarkSATHI

from selenium import webdriver 
from time import sleep
from getpass import getpass
import os
  
def connection_reseau_social(identifiant_mail, motDePasse):
    
    '''
    La fonction connection_reseau_social permet de se connecter à
    son compte de réseau social préféré.

    @paramètres
        ** identifiant_mail est de type <str>, cette chaine de caractères
        référence l identifiant de connection.
        ** motDePasse est de type <str>, cette chaine de caractères
        référence le mot de passe de connection.
        ** nom_navigateur est la methode qui permettra de naviguer 
        dans le navigateur choisi.
    @return
        Aucune valeur renvoyée.
    @effets de bord
        Modification des attributs des objets qui manipulent le navigateur.
    '''
    try :
        navigateur = webdriver.Chrome(executable_path = os.path.abspath("/usr/bin/chromedriver"))
        navigateur.implicitly_wait(30)
        navigateur.maximize_window()
        navigateur.get('https://fr-fr.facebook.com/login/') 
        champ_identification = navigateur.find_element_by_id('email') 
        champ_identification.send_keys(identifiant_mail) 
        champ_motDePasse = navigateur.find_element_by_id('pass') 
        champ_motDePasse.send_keys(motDePasse) 
        connection = navigateur.find_element_by_id('loginbutton') 
        connection.click()
        return 'connection'
    except :
        return 'Erreur de connection'

	
if __name__ == '__main__':

    import doctest
    doctest.testmod()

    connection_reseau_social('identifiant_mail', 'motDePasse')