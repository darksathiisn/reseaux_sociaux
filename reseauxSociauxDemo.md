
# **Les réseaux sociaux : un danger pour la démocratie ?**

[![acro](images/reseauxSociauxDemo.png)](https://www.dropbox.com/s/heim9qoq1q8bf1z/%5B1024x576%5D%20Les%20r%C3%A9seaux%20sociaux%20%20un%20danger%20pour%20la%20d%C3%A9mocratie%20%20-%20Vid%C3%A9o%20-%20France%20tv%20%C3%89ducation.mp4?dl=0)


Il y a à peine 10 ans, Facebook, Twitter et les autres réseaux sociaux étaient encensés dans les médias. Pourquoi ? Parce qu’ils permettaient à des militants, blogueurs et simples citoyens, d’informer, de communiquer. Et même d’organiser des mouvements sociaux jamais vus. C’est le cas en 2009 avec le « mouvement vert » en Iran, surnommé la « révolution Twitter ». Ou avec les Printemps arabes l’année suivante. 10 ans plus tard, tout a basculé. On accuse les réseaux sociaux d’avoir été utilisés pour manipuler les Américains pendant l’élection présidentielle de 2016 par exemple. Ou encore lors du referendum sur le Brexit la même année. On les accuse aussi de propager des fake news, ou de ne rien faire contre les comptes de cybermilitants qui harcèlent leurs détracteurs.

**LES ALGORITHMES**

Alors, les réseaux sociaux sont-ils devenus dangereux pour la démocratie ? Facebook, Instagram, Snapchat, Twitter… On adore y poster nos photos de vacances et nos petits mots doux. Mais ces outils sont aussi très efficaces pour propager un autre type de contenu : les fake news. Ces fausses informations sont souvent créées ou partagées par des sites militants. Leur objectif : faire de la propagande et influencer l’opinion. C’est d’autant plus efficace que les réseaux sociaux, et en particulier Facebook, sont devenus le seul mode d’information pour de nombreuses personnes. Sans qu’ils le réalisent, les usagers n’ont pas accès à des informations riches et diversifiées. Seulement à ce que l’algorithme juge le plus pertinent pour eux. Selon les préférences, la navigation, les likes, les retweets… de chacun. Les utilisateurs se retrouvent de fait enfermés dans ce que l’on appelle une « Bulle de filtre ». Une bulle cognitive dans laquelle les algorithmes ont tendance à conforter nos opinions, voire à les radicaliser. Ce qui augmente les risques d’être manipulés. Et ça, certains l’ont bien compris !

**LES VOLS DE DONNEES PERSONNELLES**

Durant la présidentielle américaine, grâce à un petit quizz comme il en existe des milliers sur les réseaux sociaux, 87 millions de profils Facebook ont été volés. L’analyse de ces données à grande échelle par l’entreprise anglaise Cambridge Analytica aurait contribué à faire élire Donald Trump. Comment ? Grâce à la création d’une campagne d’influence sur mesure. Les données hackées ont en effet permis de faire un portrait-robot très précis de millions d’utilisateurs Facebook. Cambridge Analytica a ensuite usé de techniques classiques de marketing ciblé réalisées à partir des données personnelles pour exploiter la psychologie d’une grande masse d’individus. Jouer sur leurs peurs, leurs croyances, leur idéologie… et les inciter à voter pour Trump. A cela s'ajoute le fait que le site de Mark Zuckerberg aurait été au courant du vol des données de ses utilisateurs dès 2015. Et il n’en aurait pas informé le grand public. 

Les réseaux sociaux sont donc devenus un des enjeux majeurs des campagnes électorales. Ce sont désormais des outils très puissants pour influencer l’opinion publique. Et sans garde-fou, c’est dangereux pour la démocratie.

RÉALISATEUR : Maxime Chappet

PRODUCTEUR : France Télévisions

AUTEUR : Arnaud Aubry

PRODUCTION : 2019

Publié le 28-03-2019 - Mis à jour le 08-07-2019

[**france·tv**éducation](https://education.francetv.fr/)
