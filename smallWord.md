![](http://www.vingtenaires.com/wp-content/uploads/2012/08/6-degres-de-separation.jpg)

# **Le paradoxe de Milgram : Small World**

![](images/bulleReseau.png) **Quelle hypothèse a été formulé par l'écrivain hongrois Frigyes Karinthy en 1929?**

![](https://cultura.hu/wp-content/uploads/2013/09/karinthy-frigyes2i.jpg)

![](images/bulleReseau.png) **Qui est Stanley Milgram ?**

![](http://bp2.blogger.com/_A0Pq9HDwFWY/SBixWBHQZQI/AAAAAAAAD-Y/J3rZJbKsm6Y/s320/Zakai2.jpg)

![](images/bulleReseau.png) **Recherche des informations sur "L'effet du petit mode".**

![](images/bulleReseau.png) **Explique pourquoi cette théorie à son importance pour les réseaux sociaux.** [Lien utile pour tes recherches](https://bfmbusiness.bfmtv.com/01-business-forum/reseaux-sociaux-que-le-monde-est-petit-537870.html)

![](images/bulleReseau.png) **Que représente le degré de séparation dans la "théorie du petit monde"?**

![](images/bulleReseau.png) **Donner le degré de séparation établi en 2006 par une équipe de Microsoft ayant analysé plus de 30 milliards de conversations sur Live Messenger.**

![](images/bulleReseau.png) **Donner le degré de séparation établi en 2011 par l'Université de Milan pour le réseau social Facebook.**


# ![](images/Python-Logo.png)**Graphe aléatoire d'un petit réseau social avec Python.**


```python
!pip install networkx
```

    Requirement already satisfied: networkx in c:\python36\lib\site-packages (2.4)
    Requirement already satisfied: decorator>=4.3.0 in c:\python36\lib\site-packages (from networkx) (4.4.0)
    

    WARNING: You are using pip version 19.2.3, however version 19.3.1 is available.
    You should consider upgrading via the 'python -m pip install --upgrade pip' command.
    


```python
%matplotlib inline
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
```


```python
# n ( int ) - Le nombre de nœuds.
# k ( int ) - Chaque nœud est joint à ses k voisins les plus proches dans une topologie en anneau.
# p ( float ) - La probabilité d'ajouter une nouvelle arête pour chaque arête.
# seed ( int, optionnel ) - La graine du générateur de nombres aléatoires (la valeur par défaut est None).

petit_monde = nx.newman_watts_strogatz_graph(300, 4, 0.074, seed = 12345 )
nx.draw(petit_monde)
plt.show()
```


![png](images/output_5_0.png)



```python
p = nx.shortest_path(petit_monde) 
n = petit_monde.number_of_nodes()
nb_inter = []
for i in range(n):
    for j in range(i+1,n):
        nb_inter.append(len(p[i][j])-2)
print("Nombre d'intermédiaires entre les individus du réseau")

print(f"Minimum   : {str(np.min(nb_inter))} ")
print(f"Maximum   : {str(np.max(nb_inter))} ")
print(f"Moyenne  : {str(np.mean(nb_inter))} ")
```

    Nombre d'intermédiaires entre les individus du réseau
    Minimum   : 0 
    Maximum   : 14 
    Moyenne  : 6.167714604236344 
    

![](images/bulleReseau.png) **Donner le degré de séparation de ce petit monde.**
