![logoResauxSociaux](images/logoResauxSociaux.png)
# **Les graphes et les réseaux sociaux : [Les bons Profs](https://www.lesbonsprofs.com/)**
 
Dans un réseau social, il est parfois compliqué de se représenter tous les liens pouvant exister entre les utilisateurs. On utilise en mathématiques des graphes qui permettent de représenter graphiquement les connexions qui existent dans un réseau social. 


**Le Star Wars Réseau**

![logoResauxSociaux](images/graphe.png)

On représente par un trait les liens d'amitiés entre les abonnés.

On peut alors répondre à diverses questions : 

- Si tous les utilisateurs ont des amis en commun. 
- Comment peut faire un utilisateur pour connecter un ami via d'autres amis. 
- Si tous les membres du réseau sont connectés.


## **Le vocabulaire**

- On appelle ce schéma un **graphe**.
- Un utilisateur est appelé **sommet du graphe**.
- La connexion entre deux utilisateurs est appelé une **arête** du graphe.
- La **distance** entre deux sommets est le nombre d'arêtes du chemin le plus court qui relie ces deux sommets.
- On parle de graphe **non orienté** quand la relation entre deux sommets est bilatérale.
- **L'écartement** est la distance maximale séparant un sommet du reste des sommets du graphe en empruntant le chemin le plus court.
- Le **centre** du graphe est le sommet qui a l'écartement minimal avec les autres sommets du graphe.
- Le **rayon** du graphe est l'écartement entre le centre et le sommet le plus éloigné.
- Le **diamètre** du graphe représente la distance maximale séparant les deux sommets les plus éloignés.
- Le **degré** d’un sommet d’un graphe non-orienté est le nombre d’arêtes incidentes à ce sommet.

# ![yoda](images/yoda.png) **Les questions de maître Yoda.**

![yodaPuce](images/yodaPuce.png) Combien de **sommets** a le graphe du Star Wars Réseau ?

![yodaPuce](images/yodaPuce.png) Combien d'**arêtes** a le graphe du Star Wars Réseau ?

![yodaPuce](images/yodaPuce.png) Quelle est la **distance** entre DarkVador et DarkSidious ?

![yodaPuce](images/yodaPuce.png) Donner le **centre** de ce graphe.

![yodaPuce](images/yodaPuce.png) Donner le **rayon** de ce graphe.

![yodaPuce](images/yodaPuce.png) Donner le **diamètre** de ce graphe.

![yodaPuce](images/yodaPuce.png) Quel est le **degré** d'Anakin de ce graphe.


## **Exercices** (Concours Castor)

[**Les Amis (★)**](http://castor-informatique.fr/questions/2014/2014-SI-04-unknown-friendship/index.html?options=%7B%22difficulty%22%3A%22easy%22%7D)

[**Choix des invités (★★)**](http://castor-informatique.fr/questions/2013/2013-FR-12b/)

[**Les amis de Lucie**](http://castor-informatique.fr/questions/2011/2011-SK-08/)

[**Les photos de Lucie**](http://castor-informatique.fr/questions/2011/2011-SK-11/)

-----------------------
##  ![projet](images/vadorLogo.png) **Programmation Python et les graphes.**

**Tu dois t'inspirer du code ci-dessous pour vérifier tes réponses aux questions de maître Yoda.**



```python
from networkx import nx,diameter,radius,center, pagerank
import matplotlib.pyplot as plt


sommets = ['laurent', 'pierre', 'lucie', 'sophie', 'martin', 'jacques']

aretes = [('laurent','pierre'), 
          ('sophie','lucie'), 
          ('sophie','pierre'), 
          ('martin','laurent'),
          ('pierre', 'jacques'),
          ('jacques','martin'), 
          ('jacques','laurent')]

reseau_social=nx.Graph()

reseau_social.add_nodes_from(sommets)
reseau_social.add_edges_from(aretes)
nx.draw(reseau_social, with_labels=True)
plt.show()

print(f"Le nombre de sommets de ce graphe est {reseau_social.number_of_nodes()}.")
print(f"Le nombre de arêtes de ce graphe est {reseau_social.number_of_edges()}.")
print(f"le diamètre de ce graphe est {diameter(reseau_social)}.")
print(f"Le rayon de ce graphe est {radius(reseau_social)}.")
print(f"Le centre de ce graphe est {center(reseau_social)}.")
print(f"La distance entre Lucie et Laurent est de {dict(nx.all_pairs_shortest_path_length(reseau_social))['lucie']['laurent']}.")
for sommet in sommets :
    print(f"Le degré de {sommet} est {reseau_social.degree()[sommet]}")
```


![png](images/output_21_0.png)


    Le nombre de sommets de ce graphe est 6.
    Le nombre de arêtes de ce graphe est 7.
    le diamètre de ce graphe est 4.
    Le rayon de ce graphe est 2.
    Le centre de ce graphe est ['pierre'].
    La distance entre Lucie et Laurent est de 3.
    Le degré de laurent est 3
    Le degré de pierre est 3
    Le degré de lucie est 1
    Le degré de sophie est 2
    Le degré de martin est 2
    Le degré de jacques est 3
    

## **On nommera le fichier Star_Wars_Reseau.py depuis l'IDE Thonny.**
