# Les différents réseaux sociaux

[facebook]:images/facebook.png
[instagram]:images/instagram.png
[linkedin]:images/linkedin.png
[pinterest]:images/pinterest.png
[snapchat]:images/snapchat.png
[twitter]:images/twitter.png
[whatsapp]:images/whatsapp.png
[python]:images/Python-Logo.png
[question]:images/question.png

Vous étudierez un des réseaux sociaux ci-dessous :

- **Facebook** : ![][facebook]
- **Twitter** : ![][twitter] 
- **LinkedIn** : ![][linkedin] 
- **Instagram** : ![][instagram]
- **Snapchat** : ![][snapchat] 
- **WhatsApp** : ![][whatsapp] 
- **Pinterest** : ![][pinterest] 

## Répondre aux questions suivantes :


![][question] Après avoir rappelé le principe de fonctionnement du réseau social qui vous aura été attribué, vous essayerez de trouver sur le net son nombre d'abonnées actuel (et si possible sa "dynamique de croissance" : est-ce que son nombre d'abonnées est plutôt en hausse, plutôt en baisse ou stable).

![][question] Les utilisateurs de ce réseau social ont-ils la possibilité de "régler" des "paramètres de confidentialité" afin de pouvoir préserver le respect de leur vie privée ? Si, oui, ces "paramètres de confidentialité" sont-ils facilement accessibles ?

![][question] Est-il facile de se désinscrire de ce réseau social ?

![][question] Le réseau social propose-t-il une procédure simple afin qu'un utilisateur puisse obtenir une copie des données qui auront été "récoltées" sur lui ? Si oui, la procédure est-elle simple à mettre en oeuvre ?

![][question] Le réseau social efface-t-il toutes traces des données personnelles d'un utilisateur qui s'est désinscrit ?

![][question] Enfin, vous vous intéresserez aux sources de revenus du réseau social.

**Vous devrez utiliser un traitement de texte pour rédiger vos réponses. Une fois le travail terminé, un représentant du groupe devra présenter le travail du groupe à l'ensemble de la classe.**


# [**Les réseaux sociaux : un danger pour la démocratie ?**](reseauxSociauxDemo.md)

# [**Réseaux sociaux, tous accros ?**](reseauxSociauxAcro.md)

# [**Réseaux sociaux : la manipulation par le son**](https://www.lumni.fr/video/reseaux-sociaux-la-manipulation-par-le-son)

# [**Info ou infox sur internet et les réseaux sociaux**](https://www.lumni.fr/programme/info-intox)
