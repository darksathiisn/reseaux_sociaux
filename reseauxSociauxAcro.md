
# **Réseaux sociaux, tous accros ?**

[![acro](images/reseauxSociauxAcro.png)](https://www.dropbox.com/s/xn129g8jr9wxsoh/%5B1024x576%5D%20R%C3%A9seaux%20sociaux%2C%20tous%20accros%20%20-%20Vid%C3%A9o%20-%20France%20tv%20%C3%89ducation.mp4?dl=0)

**L’addiction aux réseaux sociaux existe-t-elle ?**

Du lever au coucher, qu'on le veuille ou non, pour la plupart d'entre nous, les réseaux sociaux sont omniprésents. La majorité des 18 à 34 ans consultent leur réseau social favori dès le réveil, et plus de 80 % des adolescents consultent les réseaux sociaux avant de s'endormir... Et entre temps ?

Entre temps, on consulte les réseaux sociaux non stop ! Tout au long de la journée, un utilisateur moyen passerait un peu plus de 2 heures et 19 minutes sur les réseaux sociaux. La France se situant dans la moyenne basse avec 1 heure 23 de connexion par jour pour un utilisateur moyen de Facebook, Twitter ou Snapchat.

**ALORS, TOUS ACCROS AUX RÉSEAUX SOCIAUX ?**

Il faut dire que tout est fait pour nous faire replonger... Entre les likes, les notifications, les retweets, mais aussi l’expérience utilisateur, le design des interfaces, les couleurs, les sons, etc... tout est calculé par les ingénieurs de la Silicon Valley pour capter notre attention, rendre les plateformes addictives, et faire que nous y passions le plus de temps possible. Rien n’est laissé au hasard dans le monde merveilleux des plateformes sociales !

Mais pourquoi est-ce si difficile de résister ? Lorsque l'on reçoit un Snap, un Tweet, ou que quelqu'un aime notre nouvelle photo sur Instagram par exemple, il a été démontré que notre cerveau relâche de la dopamine, l'hormone du plaisir. Chaque notification stimule dans notre cerveau ce circuit de la récompense. Et ce sont ces notifications, qui nous rendent accros !

S'il faut rappeler que le concept d’addiction au virtuel n’est reconnu par aucune instance scientifique certains parlent de « pollution mentale par le numérique ». Ce qui pose évidemment des problèmes de santé publique. Certaines personnes ont un véritable comportement compulsif vis-à-vis des réseaux sociaux : elles vérifient de façon quasi-obsessionnelle que ce qu’elles ont posté a été commenté, liké ou encore si elles ont de nouveaux followers… Surtout que les sollicitations sont constantes à cause de l’omniprésence de notre smartphone dans notre poche.

**LES DANGERS DES RÉSEAUX SOCIAUX**

Et si le nombre de notifications baisse, cela peut entraîner des dépressions chez des personnes fragiles. Ce sont les jeunes qui y sont les plus sensibles.
Dans un article publié en 2017, le magazine américain The Atlantic a analysé l'évolution de la santé mentale des teenagers américains ces 25 dernières années. Le constat est brutal : l'omniprésence des smartphones dans les foyers américains coïncide avec une augmentation du nombre de dépressions et de pensées suicidaires. Les jeunes filles sont celles qui passent le plus de temps sur les réseaux sociaux et qui semblent être les plus fragiles. Mais les adultes ne sont pas à l'abri. Les études montrent que plus les jeunes adultes consacrent du temps aux réseaux sociaux, plus ils ressentent un sentiment de solitude...

Le tableau peut paraître sombre, mais il ne faut pas pour autant oublier que les réseaux sociaux permettent aussi de formidable découvertes, de s’informer, de rencontrer des gens passionnants, d'ouvrir une fenêtre sur le monde, de développer sa culture, son esprit, sa créativité et pour les plus timides, de mieux appréhender les relations sociales.
Les réseaux sociaux, c’est bon, mais avec modération ! Et n’oubliez pas que vous avez le droit de déconnecter.

RÉALISATEUR : Maxime Chappet

PRODUCTEUR : Corner Prod

AUTEUR : Arnaud Aubry

PRODUCTION : 2018

Publié le 11-01-2018 - Mis à jour le 08-07-2019

[**france·tv**éducation](https://education.francetv.fr/)
